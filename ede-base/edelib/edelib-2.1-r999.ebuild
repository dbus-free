# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit autotools
FEATURES="-sandbox -usersandbox -ipc-sandbox -pid-sandbox"
COMMIT="bbe3d95bc5fe5374f745c4d0558ca6ff466cac32"
DESCRIPTION="Lean FLTK based desktop enviroment"
HOMEPAGE="https://dillo.org/"
SRC_URI="https://github.com/edeproject/"${PN}"/archive/"${COMMIT}".tar.gz -> "${P}".tar.gz"
S=""${WORKDIR}"/"${PN}"-"${COMMIT}""
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="alpha amd64 arm64 mips ppc ppc64 sparc x86"
IUSE="doc"

RDEPEND="
	dev-util/ftjam
	>=x11-libs/fltk-1.3
	sys-libs/zlib
"
BDEPEND="
	${RDEPEND}
	doc? ( app-text/doxygen )
"
DOCS="AUTHORS README.md doc/*.txt"

src_prepare() {
	default
	eaclocal
	eautoheader
	eautoconf
}

src_configure() {
	econf --disable-dbus --enable-largefile --enable-fam-only --enable-shared
}

src_compile() {
	jam
	if use doc; then
		doxygen -u
		doxygen Doxyfile || die
	fi
}

src_install() {
	jam DESTDIR=/var/tmp/portage/ede-base/"${P}"/image/ install
}
