# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit toolchain-funcs
FEATURES="-sandbox -usersandbox"
DESCRIPTION="Open Sound System - applications and man pages"
HOMEPAGE="https://developer.opensound.com/"
SRC_URI="http://www.4front-tech.com/developer/sources/stable/gpl/oss-v$(ver_cut 1-2)-build2020-src-gpl.tar.bz2"

AUDIO_CARDS=( ali5455 atiaudio audigyls audioloop audiopci cmi878x cmpci cs4281
cs461x digi96 emu10k1x envy24 envy24ht fmedia geode hdaudio ich imux madi midiloop
midimix sblive sbpci sbxfi solo trident usb userdev via823x via97 ymf7xx )
DEFAULT_CARDS=( hdaudio ich imux midiloop midimix )

for card in ${AUDIO_CARDS[@]}; do
	has ${card} ${DEFAULT_CARDS[@]} &&
	CARDS=(${CARDS[@]} +oss-cards-${card}) ||
	CARDS=(${CARDS[@]} oss-cards-${card})
done
S=${WORKDIR}/${MY_P}
LICENSE="GPL-2"
SLOT="0"
BUILD_DIR=${WORKDIR}/${PN}-build
KEYWORDS="~amd64"
IUSE="alsa ogg gtk man midi vmix-fixedpoint "${CARDS[@]}""
unset CARDS
RDEPEND="gtk? ( x11-libs/gtk+ )"
DEPEND="${RDEPEND}
	app-text/txt2man
	sys-apps/makedev"

### TODO ###
# - fix linking order for --as-needed
# - fix man pages (savemixer installed twice)

src_prepare() {
	# the build system forces shadow builds
	mkdir "${BUILD_DIR}"
	cd oss-v4.2-build2020-src-gpl
	eapply "${FILESDIR}"/filesystem-layout.patch
	eapply	"${FILESDIR}"/txt2man.patch
	eapply	"${FILESDIR}"/as-needed-strip.patch
	eapply	"${FILESDIR}"/post-glibc-2.28.patch
	if use elibc_musl; then
	eapply	"${FILESDIR}"/musl.patch
	fi
	sed -e "s:GRC_MAX_QUALITY=3:GRC_MAX_QUALITY=6:" -i configure || die
	default
}

src_configure() {
	cd "${BUILD_DIR}"

	local myconf=""

	for card in ${AUDIO_CARDS[@]}; do
		if use oss_cards_${card} ||
		has ${card} ${OSS_CARDS} || has ${card} ${DEFAULT_CARDS[@]};then
			drv+=,oss_${card}
		fi
	done
	use alsa || myconf=" --enable-libsalsa=NO"
	use midi || myconf=" --config-midi=NO"
	use !vmix-fixedpoint || myconf=" --config-vmix=FIXEDPOINT"
	HOSTCC=$(tc-getCC) \
	NO_WARNING_CHECKS=1 \
	"${WORKDIR}"/oss-v4.2-build2020-src-gpl/configure \
		--only-drv=$drv \
		${myconf} || die
}

src_compile() {
	cd "${BUILD_DIR}"
if use ogg; then
	emake
fi
if use !ogg; then
	OGG_SUPPORT=NO emake
fi
	make install # this will fail, that is good until kernel modules are parted out
}

src_install() {
	cd "${BUILD_DIR}"
	mkdir /lib/modules/oss
	cp -fr "${BUILD_DIR}"/prototype/usr/lib/oss/* /lib/modules/oss/
	use alsa && dolib.so lib/libsalsa/.libs/libsalsa.so*

	dolib.so lib/libOSSlib/libOSSlib.so

	# linux-headers ships OSS3 API
	#insinto /usr/include/linux
	#doins include/soundcard.h

	# install man pages
	use gtk || rm cmd/ossxmix/ossxmix.man
	if use man; then
	rename man 1 cmd/*/*.man
	doman cmd/*/*.1
	rename .man .7 misc/man7/*.man
	doman misc/man7/*.7
	rename man 7 kernel/drv/*/*.man
	doman kernel/drv/*/*.7
	newman os_cmd/Linux/ossdetect/ossdetect.man ossdetect.8
	newman noregparm/cmd/ossdevlinks/ossdevlinks.man ossdevlinks.8
	newman noregparm/cmd/savemixer/savemixer.man savemixer.8
	newman noregparm/cmd/vmixctl/vmixctl.man vmixctl.8
	fi
	insinto /etc/oss4
	doins devices.list
	newins .version version.dat
	cat > "${ED}"/etc/oss.conf << EOF
OSSETCDIR=/etc/oss4
OSSVARDIR=/var/lib/oss4
OSSLIBDIR=/lib/modules/oss/
EOF

	cd "target"
	dosbin sbin/*
	dobin bin/*
	dolib.so lib/*

	dodir /var/lib/oss4

	newinitd "${FILESDIR}"/${PN}.init ${PN}4
	newconfd "${FILESDIR}"/${PN}.conf ${PN}4
	ewarn "Uncompiled kernel modules have been installed under /lib/modules/oss"
	ewarn "don't forget to compile them against your kernel"
	ewarn "If you want the kernel modules compiled in kernel see and execute files/oss4-inkernel.sh TODO, write ebuild instead of sh file"
	ewarn "TODO, support architectures other then linux with this ebuild"
}
