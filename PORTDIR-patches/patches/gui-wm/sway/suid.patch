--- a/include/sway/server.h	2024-04-22 18:10:42.519987316 +0000
+++ b/include/sway/server.h	2024-04-22 18:11:33.073320191 +0000
@@ -162,6 +162,8 @@
 
 extern struct sway_debug debug;
 
+/* Prepares an unprivileged server_init by performing all privileged operations in advance */
+bool server_privileged_prepare(struct sway_server *server);
 bool server_init(struct sway_server *server);
 void server_fini(struct sway_server *server);
 bool server_start(struct sway_server *server);

--- a/sway/main.c	2024-04-22 18:10:55.939987195 +0000
+++ b/sway/main.c	2024-04-22 18:11:33.076653524 +0000
@@ -150,17 +150,27 @@
 	pclose(f);
 }
 
-static bool detect_suid(void) {
-	if (geteuid() != 0 && getegid() != 0) {
-		return false;
-	}
 
-	if (getuid() == geteuid() && getgid() == getegid()) {
+static bool drop_permissions(void) {
+	if (getuid() != geteuid() || getgid() != getegid()) {
+		sway_log(SWAY_ERROR, "!!! DEPRECATION WARNING: "
+			"SUID privilege drop will be removed in a future release, please migrate to seatd-launch");
+
+		// Set the gid and uid in the correct order.
+		if (setgid(getgid()) != 0) {
+			sway_log(SWAY_ERROR, "Unable to drop root group, refusing to start");
+			return false;
+		}
+		if (setuid(getuid()) != 0) {
+			sway_log(SWAY_ERROR, "Unable to drop root user, refusing to start");
+			return false;
+		}
+	}
+	if (setgid(0) != -1 || setuid(0) != -1) {
+		sway_log(SWAY_ERROR, "Unable to drop root (we shouldn't be able to "
+			"restore it after setuid), refusing to start");
 		return false;
 	}
-
-	sway_log(SWAY_ERROR, "SUID operation is no longer supported, refusing to start. "
-			"This check will be removed in a future release.");
 	return true;
 }
 
@@ -309,11 +319,6 @@
 		}
 	}
 
-	// SUID operation is deprecated, so block it for now.
-	if (detect_suid()) {
-		exit(EXIT_FAILURE);
-	}
-
 	// Since wayland requires XDG_RUNTIME_DIR to be set, abort with just the
 	// clear error message (when not running as an IPC client).
 	if (!getenv("XDG_RUNTIME_DIR") && optind == argc) {
@@ -352,6 +357,9 @@
 					"`sway -d 2>sway.log`.");
 			exit(EXIT_FAILURE);
 		}
+		if (!drop_permissions()) {
+			exit(EXIT_FAILURE);
+		}
 		char *socket_path = getenv("SWAYSOCK");
 		if (!socket_path) {
 			sway_log(SWAY_ERROR, "Unable to retrieve socket path");
@@ -364,6 +372,16 @@
 	}
 
 	detect_proprietary(allow_unsupported_gpu);
+
+	if (!server_privileged_prepare(&server)) {
+		return 1;
+	}
+
+	if (!drop_permissions()) {
+		server_fini(&server);
+		exit(EXIT_FAILURE);
+	}
+
 	increase_nofile_limit();
 
 	// handle SIGTERM signals


--- a/sway/server.c	2024-02-24 13:39:26.000000000 +0000
+++ b/sway/server.c	2024-04-22 17:43:01.289756821 +0000
@@ -60,6 +60,18 @@
 #define SWAY_XDG_SHELL_VERSION 2
 #define SWAY_LAYER_SHELL_VERSION 4
 
+bool server_privileged_prepare(struct sway_server *server) {
+	sway_log(SWAY_DEBUG, "Preparing Wayland server initialization");
+	server->wl_display = wl_display_create();
+	server->wl_event_loop = wl_display_get_event_loop(server->wl_display);
+        server->backend = wlr_backend_autocreate(server->wl_display, &server->session);
+	if (!server->backend) {
+		sway_log(SWAY_ERROR, "Unable to create backend");
+		return false;
+	}
+	return true;
+}
+
 #if WLR_HAS_DRM_BACKEND
 static void handle_drm_lease_request(struct wl_listener *listener, void *data) {
 	/* We only offer non-desktop outputs, but in the future we might want to do
@@ -127,17 +139,9 @@
 
 bool server_init(struct sway_server *server) {
 	sway_log(SWAY_DEBUG, "Initializing Wayland server");
-	server->wl_display = wl_display_create();
-	server->wl_event_loop = wl_display_get_event_loop(server->wl_display);
 
 	wl_display_set_global_filter(server->wl_display, filter_global, NULL);
 
-	server->backend = wlr_backend_autocreate(server->wl_display, &server->session);
-	if (!server->backend) {
-		sway_log(SWAY_ERROR, "Unable to create backend");
-		return false;
-	}
-
 	server->renderer = wlr_renderer_autocreate(server->backend);
 	if (!server->renderer) {
 		sway_log(SWAY_ERROR, "Failed to create renderer");
