# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="A library for inspecting program's backtrace"
HOMEPAGE="https://www.freshports.org/devel/libexecinfo/"
SRC_URI="https://github.com/mikroskeem/libexecinfo/archive/${PV}-3.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/libexecinfo-"${PV}"-3"
LICENSE="BSD-2"
SLOT="0"
KEYWORDS="~amd64"

src_prepare() {
default
cp "${FILESDIR}"/libexecinfo-config.cmake "${S}"/
}

src_install() {
mv "${S}"/libexecinfo.so.1 "${S}"/libexecinfo.so
dolib.so "${S}"/libexecinfo.so
doheader "${S}"/execinfo.h
doheader "${S}"/stacktraverse.h
insinto "/usr/lib/cmake/libexecinfo/"
doins "${S}"/libexecinfo-config.cmake
ewarn "WARNING WARNING WARNING"
ewarn "WARNING WARNING WARNING"
ewarn "WARNING WARNING WARNING"
ewarn "Mixing libexecinfo with libs not build against libexecinfo such as on musl system will utterly fuck things up."
ewarn "Do not merge anything that does not explicitly require libexecinfo until you remove /usr/lib/libexecinfo.so.1.0 and /usr/include/execinfo.h"
ewarn "Failure to do so will result in much breakage, have a nice day"
}
