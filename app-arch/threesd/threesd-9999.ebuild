# Copyright 2019-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake git-r3 xdg

DESCRIPTION="A Nintendo 3DS file manipulator"
HOMEPAGE="https://github.com/zhaowenlan1779/threeSD"
EGIT_REPO_URI="https://github.com/zhaowenlan1779/threeSD"
LICENSE="GPL-2"
SLOT="0"

RDEPEND="
	dev-qt/qtcore
"
DEPEND="${RDEPEND}"

src_unpack() {
	git-r3_src_unpack
}

src_prepare() {
	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DUSE_SYSTEM_OPENSSL=ON
		-DUSE_SYSTEM_SDL2=ON
	)
	cmake_src_configure

}
