# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit autotools
COMMIT="9804f0e93afd42a8128c832d26b62492a5c05889"

DESCRIPTION="Lean FLTK based desktop enviroment"
HOMEPAGE="https://dillo.org/"
SRC_URI="https://github.com/edeproject/"${PN}"/archive/"${COMMIT}".tar.gz -> "${P}".tar.gz"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="alpha amd64 arm64 mips ppc ppc64 sparc x86"
IUSE="xcomposite hal"

RDEPEND="
	>=x11-libs/fltk-1.3
	ede-base/edelib
	sys-libs/zlib
"
BDEPEND="
	${RDEPEND}
"
DOCS="AUTHORS README.md doc/*.txt"

src_prepare() {
	default
	eautoreconf
}

src_configure() {
	econf  \
		$(use_enable xcomposite composite) \
		$(use_enable hal)
}

src_compile() {
	jam
}

src_install() {
	jam install
}
