## Frequently Asked Questions <a name="FAQ"></a>

- #### How do I emerge these ebuilds?

`eselect repository enable dbus-free && emerge --sync` and emerge the package just as usual.

- #### Can I use RESTRICT="mirror"?

Sure, since dbus-free packages are not mirrored on the [Gentoo mirrors](https://devmanual.gentoo.org/general-concepts/mirrors/index.html) anyway, it makes no difference. You can use RESTRICT="mirror" to avoid unnecessary fetch attempts. This is not required by dbus-free nor is it prohibited, just be sure to remove it if you want to move your package to the main Gentoo repository.

- #### I found a bug in a package that I do not maintain, and I know how to fix it, can I fix it myself?

Yes, you can! Just be sure to maintain respectful and professional behaviour.

- #### Can I commit a package without listing myself as explicit maintainer?

Yes, you can.
