# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Fork of mcl2 with focus on stability, performance and features"
HOMEPAGE="https://codeberg.org/mineclonia/mineclonia"
SRC_URI="https://codeberg.org/"${PN}"/"${PN}"/archive/"${PV}".tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/mineclonia/"
LICENSE="CC-BY-3.0 CC-BY-SA-3.0 CC-BY-SA-4.0 CC0-1.0 LGPL-2.1 LGPL-2.1+ MIT"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="games-engines/minetest"

src_install() {
	insinto /usr/share/minetest/games/${PN}
	doins -r mods menu
	doins game.conf minetest.conf
	doins screenshot.png
	dodoc README.md settingtypes.txt GROUPS.md
}
