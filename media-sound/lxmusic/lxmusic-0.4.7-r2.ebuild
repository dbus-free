# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Simple GUI XMMS2 client with minimal functionality"
HOMEPAGE="https://directory.fsf.org/wiki/LXMusic"
SRC_URI="https://master.dl.sourceforge.net/project/lxde/LXMusic%20%28music%20player%29/LXMusic%200.4.x/lxmusic-${PV}.tar.xz"
LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="
	dev-libs/glib:2
	media-sound/xmms2
	x11-libs/gdk-pixbuf:2
	x11-libs/gtk+:3"
DEPEND="${RDEPEND}"
BDEPEND="
	sys-devel/gettext
	virtual/pkgconfig"

src_configure() {
	econf --enable-gtk3
}
