# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 cmake

DESCRIPTION="A libpurple protocol plugin that adds support for the Telegram messenger"
HOMEPAGE="https://github.com/majn/telegram-purple"
EGIT_REPO_URI="https://github.com/BenWiederhake/tdlib-purple.git"

LICENSE="GPL-2+"
SLOT="0"
IUSE="gcrypt +nls +png +webp"

RDEPEND="
	x11-plugins/purple-xmpp-http-upload
	x11-plugins/Pidgin-XEP-0136-plugin
	net-libs/tdlib
	media-libs/libtgvoip
	x11-plugins/purple-carbons
	x11-plugins/pidgin-xmpp-receipts
	net-im/pidgin
	sys-libs/zlib:=
	gcrypt? ( dev-libs/libgcrypt:0= )
	!gcrypt? ( dev-libs/openssl:0= )
	png? ( media-libs/libpng:0= )
	webp? ( media-libs/libwebp:= )
"

DEPEND="${RDEPEND}"

BDEPEND="
	nls? ( sys-devel/gettext )
	virtual/pkgconfig
"

DOCS=( "README.md" )

src_prepare() {
	default

	# Remove '-Werror' to make it compile#
	find -name 'CMakeLists.txt' -exec sed -i -e 's/target_link_libraries(telegram-tdlib PRIVATE Td::TdStatic ${Purple_LIBRARIES})/target_link_libraries(telegram-tdlib PRIVATE Td::TdStatic ${Purple_LIBRARIES} ZLIB::ZLIB)\nfind_package(ZLIB)/' {} + || die
cmake_src_prepare
}

#src_configure() {
#	local myeconfargs=(
#		$(use_enable gcrypt)
#		$(use_enable nls translation)
#		$(use_enable png libpng)
#		$(use_enable webp libwebp)
#	)
#	default
#	econf "${myeconfargs[@]}"
#}
