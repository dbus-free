# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
COMMIT="20b0da9ab95d23da89f821bbddedb11b8e0e6531"
DESCRIPTION="Applies and creates IPS and BPS patches."
HOMEPAGE="https://github.com/Alcaro/Flips"
SRC_URI="https://github.com/Alcaro/Flips/archive/"${COMMIT}".tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/Flips-"${COMMIT}"/"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="x11-libs/gtk+:3
	>=dev-libs/libdivsufsort-2.0.1"
DEPEND="${RDEPEND}"

src_prepare() {
	rm -fr "${S}"/libdivsufsort-2.0.1/
	cd "${S}"/
	patch -p1 -i "${FILESDIR}"/unbundle.patch
	if use elibc_musl; then
	patch -p1 -i "${FILESDIR}"/musl.patch
	fi
	default
}

src_compile() {
	emake TARGET=gtk LFLAGS="${LDFLAGS} -ldivsufsort" || die
}

src_install() {
	dobin ${PN}
}
