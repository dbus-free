# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="library to be linked into a C/C++ program for ANSI manipulation"
HOMEPAGE="https://github.com/tsyrogit/zxcvbn-c"
SRC_URI="https://codeload.github.com/tsyrogit/${PN}/tar.gz/refs/tags/v${PV} -> ${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="amd64 ~arm ~arm64 ~mips ~ppc64 ~riscv ~x86"
IUSE="static-libs"

PATCHES=( "${FILESDIR}/31.patch" )

src_install() {

dobin "${WORKDIR}/${P}/test-c++file"
dobin "${WORKDIR}/${P}/test-c++inline"
dobin "${WORKDIR}/${P}/test-file"
dobin "${WORKDIR}/${P}/test-inline"
dobin "${WORKDIR}/${P}/test-internals"
dobin "${WORKDIR}/${P}/test-shlib"
dobin "${WORKDIR}/${P}/test-statlib"
dobin "${WORKDIR}/${P}/dictgen"
dolib.so "${WORKDIR}/${P}/libzxcvbn.so"
dolib.so "${WORKDIR}/${P}/libzxcvbn.so.0"
dolib.so "${WORKDIR}/${P}/libzxcvbn.so.0.0.0"
doheader "${WORKDIR}/${P}/zxcvbn.h"

}
