# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit qmake-utils git-r3

	EGIT_REPO_URI="https://github.com/Tuxman88/QMednafen.git"

DESCRIPTION="QT gui based frontend for mednafen"
HOMEPAGE="https://github.com/Tuxman88/QMednafen"

LICENSE="GPL-2"
SLOT="0"

DEPEND="dev-qt/qtcore
	games-emulation/mednafen
"

src_configure() {
	eqmake5
	make all
}

src_install() {
	dobin "${S}"/"${PN}"
}
