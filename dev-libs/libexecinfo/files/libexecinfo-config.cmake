find_path(libexecinfo_INCLUDE_DIR execinfo.h)
message(STATUS "libexecinfo header found at: ${libexecinfo_INCLUDE_DIR}")

find_library(libexecinfo_LIB libexecinfo.so)
message(STATUS "libexecinfo found at: ${libexecinfo_LIB}")

mark_as_advanced(libexecinfo_INCLUDE_DIR libexecinfo_LIB)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(libexecinfo REQUIRED_VARS
  libexecinfo_INCLUDE_DIR
  libexecinfo_LIB
  )

if(libexecinfo_FOUND AND NOT TARGET libexecinfo::libexecinfo)
  add_library(libexecinfo::libexecinfo SHARED IMPORTED)
  set_target_properties(libexecinfo::libexecinfo PROPERTIES
    IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
    IMPORTED_LOCATION "${libexecinfo_LIB}"
    INTERFACE_INCLUDE_DIRECTORIES
      "${libexecinfo_INCLUDE_DIR}"
    )
endif()
