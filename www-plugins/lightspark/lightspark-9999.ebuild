# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
inherit cmake git-r3 xdg-utils

DESCRIPTION="High performance flash player"
HOMEPAGE="https://lightspark.github.io/"
EGIT_REPO_URI="https://github.com/lightspark/lightspark"
S=${WORKDIR}/${P/_rc*/}
LICENSE="LGPL-3"
SLOT="0"
IUSE="cpu_flags_x86_sse2 curl ffmpeg gles nsplugin ppapi profile rtmp"

RDEPEND="app-arch/xz-utils:0=
	>=dev-libs/boost-1.42:0=
	dev-libs/glib:2=
	dev-libs/libpcre:3=[cxx]
	media-fonts/liberation-fonts
	media-libs/freetype:2=
	media-libs/libpng:0=
	media-libs/libsdl2:0=
	media-libs/sdl2-mixer:0=
	>=sys-devel/llvm-3.4:=
	sys-libs/zlib:0=
	x11-libs/cairo:0=
	x11-libs/libX11:0=
	x11-libs/pango:0=
	curl? ( net-misc/curl:0= )
	ffmpeg? (
		media-video/ffmpeg:0=
	)
	gles? ( media-libs/mesa:0=[gles2] )
	!gles? (
		>=media-libs/glew-1.5.3:0=
		virtual/opengl:0=
	)
	rtmp? ( media-video/rtmpdump:0= )"
DEPEND="${RDEPEND}
	amd64? ( dev-lang/nasm )
	x86? ( dev-lang/nasm )
	virtual/pkgconfig"

src_configure() {
	local mycmakeargs=(
		-DENABLE_CURL=$(usex curl)
		-DENABLE_GLES2=$(usex gles)
		-DENABLE_LIBAVCODEC=$(usex ffmpeg)
		-DENABLE_RTMP=$(usex rtmp)

		-DENABLE_MEMORY_USAGE_PROFILING=$(usex profile)
		-DENABLE_PROFILING=$(usex profile)
		-DENABLE_SSE2=$(usex cpu_flags_x86_sse2)

		-DCOMPILE_NPAPI_PLUGIN=$(usex nsplugin)
		-DPLUGIN_DIRECTORY="${EPREFIX}"/usr/$(get_libdir)/${PN}/plugins
		# TODO: install /etc/chromium file? block adobe-flash?
		-DCOMPILE_PPAPI_PLUGIN=$(usex ppapi)
		-DPPAPI_PLUGIN_DIRECTORY="${EPREFIX}"/usr/$(get_libdir)/chromium-browser/${PN}
	)

	cmake_src_configure
}

src_compile () {
	cmake_src_compile
}

src_install() {
	cmake_src_install

	if use nsplugin; then
		# copied from nsplugins.eclass, that's broken in EAPI 7
		dodir /usr/$(get_libdir)/nsbrowser/plugins
		dosym ../../lightspark/plugins/liblightsparkplugin.so \
			/usr/$(get_libdir)/nsbrowser/plugins/liblightsparkplugin.so
	fi
}

pkg_postinst() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}

pkg_postrm() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}
