# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit autotools desktop toolchain-funcs

DESCRIPTION="Lean FLTK based web browser"
HOMEPAGE="https://dillo.org/"
SRC_URI="
	https://github.com/"${PN}"-browser/"${PN}"/releases/download/v"${PV}"/"${P}".tar.gz
"
#	mirror://gentoo/${PN}.png
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~alpha amd64 arm arm64 ~hppa ~mips ppc ppc64 sparc x86"
IUSE="doc +gif ipv6 +jpeg +png tls"

RDEPEND="
	>=x11-libs/fltk-1.3
	sys-libs/zlib
	jpeg? ( media-libs/libjpeg-turbo )
	png? ( >=media-libs/libpng-1.2:0 )
	tls? (
		dev-libs/openssl:0=
	)
"
BDEPEND="
	${RDEPEND}
	doc? ( app-text/doxygen )
"
DOCS="AUTHORS ChangeLog README NEWS doc/*.txt doc/README"

src_prepare() {
	default
	eautoreconf
}

src_configure() {
	econf  \
		$(use_enable gif) \
		$(use_enable ipv6) \
		$(use_enable jpeg) \
		$(use_enable png) \
		$(use_enable tls)
}

src_compile() {
	emake AR="$(tc-getAR)"
	if use doc; then
		doxygen Doxyfile || die
	fi
}

src_install() {
	dodir /etc
	default

	use doc && dodoc -r html

#	doicon "${DISTDIR}"/${PN}.png
	make_desktop_entry ${PN} Dillo
}
