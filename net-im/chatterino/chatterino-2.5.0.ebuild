# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake optfeature xdg-utils

DESCRIPTION="Chat client for https://twitch.tv"
HOMEPAGE="https://chatterino.com/"
SRC_URI="
	https://github.com/Chatterino/chatterino2/archive/v"${PV}".tar.gz -> "${P}".tar.gz
	https://github.com/Chatterino/libcommuni/archive/030710a.tar.gz -> libcommuni-030710a.tar.gz
	https://github.com/Neargye/magic_enum/archive/e55b9b5.tar.gz -> magic_enum-e55b9b5.tar.gz
	https://github.com/mackron/miniaudio/archive/4a5b74b.tar.gz -> miniaudio-4a5b74b.tar.gz
	https://github.com/Chatterino/qtkeychain/archive/e5b070831cf1ea3cb98c95f97fcb7439f8d79bd6.tar.gz -> qtkeychain-e5b0708.tar.gz
	https://github.com/Tencent/rapidjson/archive/d87b698.tar.gz -> rapidjson-d87b698.tar.gz
	https://github.com/pajlada/serialize/archive/17946d6.tar.gz -> serialize-17946d6.tar.gz
	https://github.com/pajlada/settings/archive/70fbc72.tar.gz -> settings-70fbc72.tar.gz
	https://github.com/pajlada/signals/archive/d067706.tar.gz -> signals-d067706.tar.gz
	https://github.com/zaphoyd/websocketpp/archive/b9aeec6.tar.gz -> websocketpp-b9aeec6.tar.gz
	https://github.com/arsenm/sanitizers-cmake/archive/3f0542e.tar.gz -> sanitizers-cmake-3f0542e.tar.gz
"

S="${WORKDIR}"/chatterino2-"${PV}"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="
	dev-libs/openssl:=
	dev-qt/qtconcurrent
	dev-qt/qtgui
	dev-qt/qtmultimedia
	dev-qt/qtnetwork
	dev-qt/qtsvg
	dev-qt/qtwidgets
	dev-qt/qtimageformats
"
DEPEND="
	${RDEPEND}
	dev-libs/boost
"
BDEPEND="dev-qt/linguist-tools:5"

src_prepare() {
	rmdir --ignore-fail-on-non-empty ./lib/*/ ./cmake/*/ || die
	ln -sr ../libcommuni-* ./lib/libcommuni || die
	ln -sr ../magic_enum-* ./lib/magic_enum || die
	ln -sr ../miniaudio-* ./lib/miniaudio || die
	ln -sr ../rapidjson-* ./lib/rapidjson || die
	ln -sr ../qtkeychain-* ./lib/qtkeychain || die
	ln -sr ../serialize-* ./lib/serialize || die
	ln -sr ../settings-* ./lib/settings || die
	ln -sr ../signals-* ./lib/signals || die
	ln -sr ../websocketpp-* ./lib/websocketpp || die
	ln -sr ../sanitizers-cmake-* ./cmake/sanitizers-cmake || die
	cd ..
	eapply "${FILESDIR}"/dbus-removal-1.patch
	find qtkeychain-e5b070831cf1ea3cb98c95f97fcb7439f8d79bd6/keychain_p.h -type f -print0 | xargs -0 sed -i -e 's/defined(Q_OS_UNIX)/!defined(Q_OS_UNIX)/g'
	cp "${FILESDIR}"/keychain_p.h qtkeychain-e5b070831cf1ea3cb98c95f97fcb7439f8d79bd6/ || die
	eapply "${FILESDIR}"/dbus-removal-2.patch
	default
	cd "${S}"
	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DCHATTERINO_PLUGINS=OFF
		-DUSE_SYSTEM_QTKEYCHAIN=OFF
	)
	cmake_src_configure
}

src_install() {
	dobin "${WORKDIR}"/"${PN}"2-"${PV}"_build/bin/chatterino
	dolib.so "${WORKDIR}"/"${PN}"2-"${PV}"_build/lib/qtkeychain/libqt5keychain.so.0.13.99
	dolib.so "${WORKDIR}"/"${PN}"2-"${PV}"_build/lib/qtkeychain/libqt5keychain.so.1
	dolib.so "${WORKDIR}"/"${PN}"2-"${PV}"_build/lib/libcommuni/libLibCommuni.so
	default
	cmake_src_install
	mv "${D}"/usr/share/icons/hicolor/256x256/apps/{com.chatterino.,}chatterino.png || die
}

pkg_postinst() {
	xdg_icon_cache_update
	optfeature "for opening streams in a local video player" net-misc/streamlink
}

pkg_postrm() {
	xdg_icon_cache_update
}
