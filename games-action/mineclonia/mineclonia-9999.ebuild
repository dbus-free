# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit git-r3
DESCRIPTION="Fork of mcl2 with focus on stability, performance and features"
HOMEPAGE="https://codeberg.org/mineclonia/mineclonia"
EGIT_REPO_URI="https://codeberg.org/"${PN}"/"${PN}".git"

LICENSE="CC-BY-3.0 CC-BY-SA-3.0 CC-BY-SA-4.0 CC0-1.0 LGPL-2.1 LGPL-2.1+ MIT"
SLOT="0"
RDEPEND="
minetest? ( games-engines/minetest )
luanti? ( games-engines/luanti )
"
IUSE="luanti minetest"
src_install() {
	if use minetest; then
	insinto /usr/share/minetest/games/${PN}
	fi
	if use luanti; then
	insinto /usr/share/luanti/games/${PN}
	fi
	doins -r mods menu
	doins game.conf minetest.conf
	doins screenshot.png
	dodoc README.md settingtypes.txt GROUPS.md
}
