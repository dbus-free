Why do I have to fork the whole gentoo ebuild tree to have static libs to build a GUI elf program? Just why
aren't the options put into the ebuilds by default? I see very few, *cough* mesa *cough*, packages that don't have any options for
static libs being built into the build scripts. But just about every other does, yet it's not exposed or is forced into shared mode in the ebuild.

Very longterm goal is to have a working GUI web browser and desktop without needing dev-libs/glib installed
QT as a toolkit is supposedly able to be used without glib via an enviroment variable according to their ancient docs
so now for the web browser, desktop software, and all its dependencies...
