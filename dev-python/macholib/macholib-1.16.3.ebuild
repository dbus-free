# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{10..12} )
inherit distutils-r1
#Tests mostly fail and we should look into that

DESCRIPTION="A package is a collection of utilities for dealing with IP addresses"
HOMEPAGE="https://github.com/ronaldoussoren/macholib"
SRC_URI="https://github.com/ronaldoussoren/macholib/archive/refs/tags/v"${PV}".tar.gz -> "${P}".tar.gz"
LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~arm64 x86"
RESTRICT=test

RDEPEND="${PYTHON_DEPS}
	dev-python/altgraph[${PYTHON_USEDEP}]"
DEPEND="${RDEPEND}"
